AC_INIT([rtmpdump], [2.4.0],
        [http://rtmpdump.muplayer.hu])
dnl === Shared-library version
SHLIBVERSION=2:4:0
AC_SUBST(SHLIBVERSION)
AC_CANONICAL_HOST
AC_PREREQ([2.60])
AM_INIT_AUTOMAKE([-Wall foreign subdir-objects])

dnl === automake >= 1.12 requires this for 'unusual archivers' support.
dnl === it must occur before LT_INIT (AC_PROG_LIBTOOL).
m4_ifdef([AM_PROG_AR], [AM_PROG_AR])

AC_PROG_LIBTOOL
AM_PROG_CC_C_O
AC_PROG_INSTALL

dnl === Enable less verbose output when building.
m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([yes])])

dnl == test endianness
AC_C_BIGENDIAN

dnl === SET_IF_UNSET(shell_var, value)
dnl ===   Set the shell variable 'shell_var' to 'value' if it is unset.
AC_DEFUN([SET_IF_UNSET], [test "${$1+set}" = "set" || $1=$2])

AC_ARG_ENABLE([everything],
              AS_HELP_STRING([--enable-everything],
                             [Enable all optional targets. These can still be
                              disabled with --disable-target]),
              [SET_IF_UNSET([enable_tools], [$enableval])
               SET_IF_UNSET([enable_docs], [$enableval])])

AC_ARG_WITH([pkgconfigdir], AS_HELP_STRING([--with-pkgconfigdir=DIR],
            [Path to the pkgconfig directory @<:@LIBDIR/pkgconfig@:>@]),
            [pkgconfigdir="$withval"], [pkgconfigdir='${libdir}/pkgconfig'])
AC_SUBST([pkgconfigdir])

dnl === Check for Crypto support
dnl === LIBCHECK_PROLOGUE([OPENSSL])
AC_CHECK_HEADER([openssl/ssl.h],
  ssl_provider="OpenSSL",
  AC_MSG_ERROR(OpenSSL library not available)
)
AC_CHECK_HEADER([openssl/md5.h],,
  AC_MSG_ERROR(OpenSSL libcrypto library not available)
)
AC_CHECK_HEADER([zlib.h],,
  AC_MSG_ERROR(Zlib not available)
)
dnl === LIBCHECK_EPILOGUE([OPENSSL])

dnl === TEST_AND_ADD_CFLAGS(var, flag)
dnl ===   Checks whether $CC supports 'flag' and adds it to 'var'
dnl ===   on success.
AC_DEFUN([TEST_AND_ADD_CFLAGS],
         [SAVED_CFLAGS="$CFLAGS"
          CFLAGS="-Werror $2"
          AC_MSG_CHECKING([whether $CC supports $2])
          dnl Note AC_LANG_PROGRAM([]) uses an old-style main definition.
          AC_COMPILE_IFELSE([AC_LANG_SOURCE([int main(void) { return 0; }])],
                            [AC_MSG_RESULT([yes])]
                            dnl Simply append the variable avoiding a
                            dnl compatibility ifdef for AS_VAR_APPEND as this
                            dnl variable shouldn't grow all that large.
                            [$1="${$1} $2"],
                            [AC_MSG_RESULT([no])])
          CFLAGS="$SAVED_CFLAGS"])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wall])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wdeclaration-after-statement])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wextra])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wformat-nonliteral])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wformat-security])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wmissing-declarations])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wmissing-prototypes])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wold-style-definition])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wshadow])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wunused-but-set-variable])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wunused])
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wvla])
dnl Adding this one just to make the build a bit more bearable, but the crypto calls do need updating
TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-Wno-deprecated-declarations])
# https://gcc.gnu.org/bugzilla/show_bug.cgi?id=62040
# https://gcc.gnu.org/bugzilla/show_bug.cgi?id=61622
AS_IF([test "$GCC" = "yes" ], [
       gcc_version=`$CC -dumpversion`
       gcc_wht_bug=""
       case "$host_cpu" in
         aarch64|arm64)
          case "$gcc_version" in
            4.9|4.9.0|4.9.1) gcc_wht_bug=yes ;;
          esac
       esac
       AS_IF([test "$gcc_wht_bug" = "yes"], [
              TEST_AND_ADD_CFLAGS([AM_CFLAGS], [-frename-registers])])])
AC_SUBST([AM_CFLAGS])

dnl === CLEAR_LIBVARS([var_pfx])
dnl ===   Clears <var_pfx>_{INCLUDES,LIBS}.
AC_DEFUN([CLEAR_LIBVARS], [$1_INCLUDES=""; $1_LIBS=""])

dnl === WITHLIB_OPTION([opt_pfx], [outvar_pfx])
dnl ===   Defines --with-<opt_pfx>{include,lib}dir options which set
dnl ===   the variables <outvar_pfx>_{INCLUDES,LIBS}.
AC_DEFUN([WITHLIB_OPTION],
  [AC_ARG_WITH([$1includedir],
               AS_HELP_STRING([--with-$1includedir=DIR],
                              [use $2 includes from DIR]),
               $2_INCLUDES="-I$withval")
   AC_ARG_WITH([$1libdir],
               AS_HELP_STRING([--with-$1libdir=DIR],
                              [use $2 libraries from DIR]),
               [$2_LIBS="-L$withval"])])

dnl === LIBCHECK_PROLOGUE([var_pfx])
dnl ===   Caches the current values of CPPFLAGS/LIBS in SAVED_* then
dnl ===   prepends the current values with <var_pfx>_{INCLUDES,LIBS}.
AC_DEFUN([LIBCHECK_PROLOGUE],
         [SAVED_CPPFLAGS=$CPPFLAGS
          SAVED_LIBS=$LIBS
          CPPFLAGS="$$1_INCLUDES $CPPFLAGS"
          LIBS="$$1_LIBS $LIBS"])

dnl === LIBCHECK_EPILOGUE([var_pfx])
dnl ===   Restores the values of CPPFLAGS/LIBS from SAVED_* and exports
dnl ===   <var_pfx>_{INCLUDES,LIBS} with AC_SUBST.
AC_DEFUN([LIBCHECK_EPILOGUE],
         [AC_SUBST($1_LIBS)
          AC_SUBST($1_INCLUDES)
          CPPFLAGS=$SAVED_CPPFLAGS
          LIBS=$SAVED_LIBS])

dnl === Check whether rtmpdump tools should be built
AC_MSG_CHECKING(whether rtmpdump tools is to be built)
AC_ARG_ENABLE([tools],
              AS_HELP_STRING([--enable-tools],
                             [Build rtmpdump tools (rtmpdump, signswf, etc) @<:@default=no@:>@]))
AC_MSG_RESULT(${enable_tools-no})
AM_CONDITIONAL([WANT_TOOLS], [test "$enable_tools" = "yes"])

dnl === Check whether rtmpdump docs should be built
AC_MSG_CHECKING(whether rtmpdump docs is to be built)
AC_ARG_ENABLE([docs],
              AS_HELP_STRING([--enable-docs],
                             [Build rtmpdump docs @<:@default=no@:>@]))
AC_MSG_RESULT(${enable_docs-no})
AM_CONDITIONAL([WANT_DOCS], [test "$enable_docs" = "yes"])

dnl =========================

AC_CONFIG_MACRO_DIR([m4])
dnl AC_CONFIG_HEADERS([librtmp/config.h])
AC_CONFIG_FILES([Makefile librtmp/Makefile \
                 librtmp/librtmp.pc \
                 ])


AC_OUTPUT

AC_MSG_NOTICE([
RTMP Configuration Summary
--------------------------

Shared libraries: ${enable_shared}
Static libraries: ${enable_static}
Crypto Libraries: ${ssl_provider}

Subcomponents
-------------

librtmp: yes
tools: ${enable_tools-no}
docs: ${enable_docs-no}
])
